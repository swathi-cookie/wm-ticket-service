
package com.walmart.ticket.system.test;

/**
 * Created by spaladugu on 6/5/2018.
 */

import com.walmart.ticket.system.model.SeatHold;
import com.walmart.ticket.system.model.Venue;
import com.walmart.ticket.system.service.impl.TicketServiceImpl;

import org.junit.*;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class TicketServiceImplTest {
    private TicketServiceImpl service;
    private int second = 10;
    private int wait = 15;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        service = new TicketServiceImpl(new Venue(1, 1), second);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void numSeatsAvailable() throws InterruptedException {
        int no = service.numSeatsAvailable();
        assert (no == 1);
        Venue v = new Venue(2, 3);
        service = new TicketServiceImpl(v, second);
        no = service.numSeatsAvailable();
        assert (no == (2 * 3));
        service.findAndHoldSeats(2, "hold.reserve@ts.com");
        no = service.numSeatsAvailable();
        assert (no == ((2 * 3) - 2));
        TimeUnit.SECONDS.sleep(wait); // default expire time is 10s. prior hold should be gone.
        System.out.println("After waiting: " + service.numSeatsAvailable());
        assert ((2 * 3) == service.numSeatsAvailable());
        SeatHold sh = service.findAndHoldSeats(3, "hold.reserve@ts.com");
        assert (3 == service.numSeatsAvailable());
        service.reserveSeats(sh.getId(), "hold.reserve@ts.com");
        TimeUnit.SECONDS.sleep(wait);// default expire time is 10s. prior hold should be gone.
        assertFalse((2 * 3) == service.numSeatsAvailable());
        System.out.println("After waiting: " + v.getSeatsPerRow());
        service.findAndHoldSeats(6, "hold2.reserve@ts.com");
        no = service.numSeatsAvailable();
        assert (no == service.numSeatsAvailable()); // reserving seats should not change no of seats. (assuming we don`t pass expiry)
        System.out.println("No. seats test cases completed");

    }

    @Test
    public void findAndHoldSeats() throws InterruptedException {
        SeatHold s1 = service.findAndHoldSeats(1, "hold.reserve@ts.com");
        assertNotNull(s1);
        assert (1 == s1.getSeatsHeld().size());
        s1 = service.findAndHoldSeats(1, "hold.reserve@ts.com");
        assert (null == s1);
        TimeUnit.SECONDS.sleep(wait);
        s1 = service.findAndHoldSeats(1, "hold.reserve@ts.com");
        assert (1 == s1.getSeatsHeld().size());
        TimeUnit.SECONDS.sleep(wait);
        s1 = service.findAndHoldSeats(2, "hold.reserve@ts.com");
        assert (null == s1);
    }

    @Test
    public void reserveSeats() throws InterruptedException {
        SeatHold s1 = service.findAndHoldSeats(1, "hold.reserve@ts.com");
        String confId = service.reserveSeats(s1.getId(), "hold.reserve@ts.com");
        assertNotNull(confId);
        assertTrue(confId.contains("reserved!"));
        confId = service.reserveSeats(0, "hold.reserve@ts.com");
        assert (null == confId);
    }

    @Test
    public void hold_noSeatsAvailable() {
        final int seats = 0;
        final int rows = 0;
        final long secs = 5;
        Venue venue = new Venue(seats, rows);
        this.service = new TicketServiceImpl(venue, secs);
        SeatHold hold = service.findAndHoldSeats(3, "hold.noseats@ts.com");
        assert (null == hold);
    }

    @Test
    public void holdSeats_notEnoughAvailable() {
        final int seats = 2;
        final int rows = 2;
        Venue venue = new Venue(seats, rows);
        this.service = new TicketServiceImpl(venue, 10);
        SeatHold hold = service.findAndHoldSeats(5, "hold.notenough@ts.com");
        assert (null == hold);
    }


}
