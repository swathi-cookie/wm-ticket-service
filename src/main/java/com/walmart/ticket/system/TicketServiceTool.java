package com.walmart.ticket.system;

/**
 * Created by spaladugu on 6/1/2018.
 */

import com.walmart.ticket.system.model.SeatHold;
import com.walmart.ticket.system.model.Venue;
import com.walmart.ticket.system.service.TicketService;
import com.walmart.ticket.system.service.impl.TicketServiceImpl;
import com.walmart.ticket.system.utils.ScanInputValidation;
import com.walmart.ticket.system.utils.SeatScore;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TicketServiceTool {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketServiceTool.class.getName());

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("\t\t\tStarted TicketService System");
        System.out.println("\t\t\t ====================");
        LOGGER.info("Started TicketService System");
        boolean loop = true;
        String options = "\nOptions: \t1. Start/Reset Venue \t2. Available Venue Seats \t3. Request for Hold \t4. Reserve Seats \t5. Exit.";
        LOGGER.info(options);
        int rows = 5;
        int seatsProw = 5;
        SeatScore scorer = new SeatScore();
        Venue v = new Venue(rows, seatsProw);

        TicketService service = new TicketServiceImpl(v);
        System.out.println("System started with " + rows + " rows & " + seatsProw + " seats per row venue! (Expiration seconds is set to 100 secs.))");
        LOGGER.info("System started with " + rows + " rows & " + seatsProw + " seats per row venue! (Expiration seconds is set to 100 secs.))");
        while (loop) {
            System.out.println(options);
            String str = sc.next();
            boolean isvalidInput = ScanInputValidation.isValidNo(str);
            if (!isvalidInput) {
                System.out.println("Select only numbers.");
                LOGGER.info("Select only numbers.");
                continue;
            }
            int input = Integer.parseInt(str);
            switch (input) {
                case 1:
                    System.out.println("How many rows?");
                    LOGGER.info("How many rows?");
                    String vrows = sc.next();
                    boolean isvalidRow = ScanInputValidation.isValidNo(vrows);
                    if (!isvalidRow) {
                        while (!isvalidRow) {
                            System.out.println("Invalid row no.");
                            LOGGER.error("Invalid row no.");
                            System.out.println("Enter valid no:");
                            LOGGER.info("Enter valid no:");
                            vrows = sc.next();
                            isvalidRow = ScanInputValidation.isValidNo(vrows);
                        }
                    }
                    rows = Integer.parseInt(vrows);
                    System.out.println("How many seats per rows?");
                    LOGGER.info("How many seats per rows?");
                    String seatPRow = sc.next();
                    boolean isvalidStPRw = ScanInputValidation.isValidNo(seatPRow);
                    if (!isvalidStPRw) {
                        while (!isvalidStPRw) {
                            System.out.println("Invalid seat no.");
                            LOGGER.error("Invalid seat no.");
                            System.out.println("Enter valid no:");
                            LOGGER.info("Enter valid no");
                            seatPRow = sc.next();
                            isvalidStPRw = ScanInputValidation.isValidNo(seatPRow);
                        }
                    }
                    seatsProw = Integer.parseInt(seatPRow);
                    LOGGER.info("Seats Per Row: " + seatPRow);
                    System.out.println("Expiration seconds (Optional: Hit enter to skip)");
                    LOGGER.info("Expiration seconds (Optional: Hit enter to skip)");
                    int exp;
                    try {
                        exp = Integer.parseInt(sc.next());
                    } catch (NumberFormatException e) {
                        exp = -1;
                    }
                    v = new Venue(rows, seatsProw);
                    service = (exp == -1) ? new TicketServiceImpl(v) : new TicketServiceImpl(v, exp);
                    System.gc();
                    break;
                case 2:
                    int availSeats = service.numSeatsAvailable();
                    System.out.println("\nNo of seats available now: " + availSeats);
                    LOGGER.info("\nNo of seats available now: " + availSeats);
                    break;
                case 3:
                    System.out.println("How many seats for hold?");
                    LOGGER.info("How many seats for hold?");
                    String hs = sc.next();
                    boolean isvalidSeat = ScanInputValidation.isValidNo(hs);
                    if (!isvalidSeat) {
                        while (!isvalidSeat) {
                            System.out.println("Invalid seat no.");
                            LOGGER.error("Invalid seat no." + hs);
                            System.out.println("Enter valid no:");
                            hs = sc.next();
                            LOGGER.info("Enter valid no:" + hs);
                            isvalidSeat = ScanInputValidation.isValidNo(hs);
                        }
                    }
                    int seats = Integer.parseInt(hs);
                    System.out.println("Customer email?");
                    LOGGER.info("Customer email?");
                    String email = sc.next();
                    boolean isvalid = ScanInputValidation.isValidEmail(email);
                    if (!isvalid) {
                        while (!isvalid) {
                            System.out.println("Invalid email pattern.");
                            LOGGER.error("Invalid email pattern." + email);
                            System.out.println("Enter valid email:");

                            email = sc.next();
                            LOGGER.info("Enter valid email:" + email);
                            isvalid = ScanInputValidation.isValidEmail(email);
                        }
                    }
                    SeatHold hold = service.findAndHoldSeats(seats, email);
                    if (hold != null) {
                        System.out.println("\n" + seats + " held!\n" + hold);
                        LOGGER.info("\n" + seats + " held!\n" + hold);
                    } else {
                        System.out.println("\nYour request has been failed! Please try again!");
                        LOGGER.info("Your request has been failed! Please try again!");
                    }
                    break;
                case 4:
                    System.out.println("SeatHold Id?");
                    LOGGER.info("SeatHold Id?");
                    String id = sc.next();
                    boolean isvalidDigit = ScanInputValidation.isValidNo(id);
                    if (!isvalidDigit) {
                        while (!isvalidDigit) {
                            System.out.println("Invalid no pattern.");
                            LOGGER.info("Invalid no pattern." + id);
                            System.out.println("Enter valid no:");

                            id = sc.next();
                            LOGGER.info("Enter valid no:" + id);
                            isvalidDigit = ScanInputValidation.isValidNo(id);
                        }
                    }
                    int holdId = Integer.parseInt(id);
                    System.out.println("Associated with which customer email?");
                    String cust = sc.next();
                    LOGGER.info("Associated customer email?" + cust);
                    boolean isvalidEmail = ScanInputValidation.isValidEmail(cust);
                    if (!isvalidEmail) {
                        while (!isvalidEmail) {
                            System.out.println("Invalid email pattern.");
                            LOGGER.error("Invalid email pattern." + cust);
                            System.out.println("Enter valid email:");

                            cust = sc.next();
                            LOGGER.info("Enter valid email:" + cust);
                            isvalidEmail = ScanInputValidation.isValidEmail(cust);
                        }
                    }
                    System.out.println("\n" + service.reserveSeats(holdId, cust));
                    break;
                case 5:
                    loop = false;
                    System.out.println("\nYou requested to exit from venue");
                    LOGGER.info("\nYou requested to exit from venue");
                    break;
                default:
                    System.out.println("Invalid option.");
                    LOGGER.info("You provided an Invalid option.");
            }
        }
        sc.close();


    }

}
