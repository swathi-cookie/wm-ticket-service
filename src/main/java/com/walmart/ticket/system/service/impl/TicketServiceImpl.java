package com.walmart.ticket.system.service.impl;

/**
 * Created by spaladugu on 6/1/2018.
 */

import com.walmart.ticket.system.model.*;
import com.walmart.ticket.system.service.TicketService;
import com.walmart.ticket.system.utils.ScanInputValidation;
import com.walmart.ticket.system.utils.SeatHoldUtils;
import com.walmart.ticket.system.utils.SeatScore;

import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

public class TicketServiceImpl implements TicketService {
    private int available;
    private Venue v;
    private Map<Integer, SeatHold> seatHoldMapper;
    private long seconds = 100L;
    private static final Logger LOGGER = Logger.getLogger(TicketServiceImpl.class.getName());
    private SeatScore scorer;
    private PriorityQueue<SeatBlock> seatBlocks = new PriorityQueue<>();


    public TicketServiceImpl(Venue v) {
        super();
        this.v = v;
        this.available = v.getCapacity();
        seatHoldMapper = new TreeMap<Integer, SeatHold>();
        this.seatBlocks = getSeatBlocks();
    }

    private PriorityQueue<SeatBlock> getSeatBlocks() {
        Seat[][] seats = v.getSeats();

        final int rowSize = v.getSeatsPerRow();
        final int rows = v.getRows();
        for (int row = 0; row < rows; row++) {
            List<Seat> storeSeats = new ArrayList<>(rowSize);

            for (int seat = 0; seat < rowSize; seat++) {
                Seat st = seats[row][seat];
                /*float score = this.scorer.calculateScore(seat, row, this.v.getRows(), this.v.getSeatsPerRow());
                score = SeatHoldUtils.round(score);*/

                storeSeats.add(st);

            }

            this.seatBlocks.add(new SeatBlock(storeSeats));
        }

        return this.seatBlocks;
    }

    public TicketServiceImpl(Venue v, long secs) {
        this(v);
        this.seconds = secs;
        this.scorer = scorer;
        seatHoldMapper = new TreeMap<Integer, SeatHold>();

        assert (this.seconds > 0);
        this.seatBlocks = getSeatBlocks();


    }

    public int numSeatsAvailable() {
        expiryCheck();
        System.out.println(v.prettyPrint());
        return available;
    }

    private void expiryCheck() {
        for (Iterator<Map.Entry<Integer, SeatHold>> it = seatHoldMapper.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<Integer, SeatHold> entry = it.next();

            SeatHold tempSH = entry.getValue();
            long now = Instant.now().getEpochSecond();
            if ((now - tempSH.getCreatedAt().getEpochSecond()) > this.seconds) {
                updateStatus(tempSH.getSeatsHeld(), STATUS.AVAILABLE);
                this.seatBlocks.add(new SeatBlock(tempSH.getSeatsHeld()));

                this.available += tempSH.getSeatsHeld().size();
                it.remove();
            }
        }
    }

    private void expiryCheck(int seatHoldId) {
        SeatHold tempSH = seatHoldMapper.get(seatHoldId);
        if (tempSH != null) {
            long now = Instant.now().getEpochSecond();
            if ((now - tempSH.getCreatedAt().getEpochSecond()) > this.seconds) {
                updateStatus(tempSH.getSeatsHeld(), STATUS.AVAILABLE);
                this.available += tempSH.getSeatsHeld().size();
                seatHoldMapper.remove(seatHoldId);
            }
        }
    }

    public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
        expiryCheck();
        List<Seat> holdingSeats = findBestSeats(numSeats);
        updateStatus(holdingSeats, STATUS.HOLD);
        this.available -= holdingSeats.size();
        SeatHold hold = generateSeatHold(holdingSeats, customerEmail);
        if (hold != null) seatHoldMapper.put(hold.getId(), hold);
        return hold;
    }

    private void updateStatus(List<Seat> seats, STATUS status) {
        for (Seat st : seats) {
            st.setStatus(status);
        }
    }

    private SeatHold generateSeatHold(List<Seat> holdingSeats, String customerEmail) {
        if (holdingSeats.size() < 1) {
            return null;
        }
        SeatHold hold = new SeatHold();
        hold.setCustomer(new Customer(customerEmail));
        hold.setSeatsHeld(holdingSeats);
        hold.setCreatedAt(Instant.now());

        return hold;
    }


    public String reserveSeats(int seatHoldId, String customerEmail) {
        expiryCheck(seatHoldId);
        SeatHold seatHold = finder(seatHoldId);
        if (seatHold == null) {
            System.out.println("Either seatHoldId is invalid OR is expired! ");
            return null;
        }
        boolean isValidCustomer = ScanInputValidation.validateCustomer(customerEmail, seatHold.getCustomer().getEmail());
        if (!isValidCustomer) {
            return "cannot verify customer. Please request reservation with correct customer email.";
        }
        updateStatus(seatHold.getSeatsHeld(), STATUS.RESERVED);
        String result = ScanInputValidation.reservationCode(seatHold);
        seatHoldMapper.remove(seatHoldId);
        return result;
    }

    private SeatHold finder(int seatHoldId) {
        return seatHoldMapper.get(seatHoldId);
    }

    /**
     * Find the best available seats from the currently available seats.
     *
     * @param numSeats - the number of seats requested.
     * @return the best available Seats from queue to fulfill the order or empty list if
     * it cannot be fulfilled.
     */
    private List<Seat> findBestSeats(int numSeats) {
        int seatsRequired = numSeats;
        List<Seat> heldSeats = new ArrayList<>();
        List<SeatBlock> usedBlocks = new ArrayList<>();
        if (this.available < numSeats) {
            System.out.println("There are only " + this.available + " seats available now!");
            return new LinkedList<Seat>();
        } else if (numSeats > this.v.getSeatsPerRow()) {
            System.out.println("You can only block " + this.v.getSeatsPerRow() + " seats at a time!");
            return new LinkedList<Seat>();
        }

        for (SeatBlock block : this.seatBlocks) {
            if (block.size() >= numSeats) {
                usedBlocks.add(block);

                if (block.size() == seatsRequired) {
                    // The block exactly matches to the requested seats.
                    // Adds the entire block to the result.
                    for (Seat seat : block.getSeats()) {
                        if (seat.getStatus() == STATUS.AVAILABLE) {
                            heldSeats.add(seat);
                            seatsRequired--;
                        }
                    }
                    /*heldSeats.addAll(block.getSeats());
                    seatsRequired -= block.size();*/
                    if (seatsRequired == 0) {
                        break;
                    }
                } else if (seatsRequired < block.size()) {
                    // This block has more seats than requested seats
                    // Adds the best available split to holdingSeats.
                    List<SeatBlock> splits = block.split(seatsRequired);
                    if(splits.size()>0) {
                        SeatBlock bestAvailableBlock = splits.get(0);
                        for (Seat seat : bestAvailableBlock.getSeats()) {
                            if (seat.getStatus() == STATUS.AVAILABLE) {
                                heldSeats.add(seat);
                                seatsRequired--;
                            }
                        }
                    }
                    // we were adding only the best block to heldseats.Hence the other splits should be added back to Queue.
                    if (seatsRequired == 0) {
                        for (int i = 1; i < splits.size(); i++) {
                            this.seatBlocks.add(splits.get(i));
                        }

                        break;
                    }

                }
            } // no block is a match, keeps searching.
        }

        // We remove the blocks that were held successfully from Queue
        if (usedBlocks.size() > 0) {
            for (SeatBlock toDelete : usedBlocks) {
                this.seatBlocks.remove(toDelete);
            }
        }

        // Returns the heldseats.
        return heldSeats;
    }

}
