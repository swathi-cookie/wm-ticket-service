package com.walmart.ticket.system.utils;

/**
 * Created by spaladugu on 6/1/2018.
 */

import com.walmart.ticket.system.model.Seat;
import com.walmart.ticket.system.model.SeatHold;


import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScanInputValidation {
    public static boolean isValidEmail(String email) {
        if (email == null || "".equals(email))
            return false;

        email = email.trim();


        return validate(email);
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean isValidNo(String no) {
        if (no == null) {
            return false;
        }
        try {
            Integer.parseInt(no);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean validateCustomer(String input_customer, String stored_customer) {
        if (input_customer == null || stored_customer == null) {
            return false;
        }
        return input_customer.equalsIgnoreCase(stored_customer);
    }

    public static String reservationCode(SeatHold hold) {
        StringBuilder sb = new StringBuilder();
        sb.append("Congrats! Your seats have been reserved!\n");
        sb.append("Details:\n");
        sb.append("Confirmation no: " + UUID.randomUUID().toString() + "\n");
        sb.append("seats: [ ");
        for (Seat st : hold.getSeatsHeld()) {
            sb.append(st.getSeatNo());//.getSeatNo());
            sb.append(" ");
        }
        sb.append("]");
        return sb.toString();
    }
}
