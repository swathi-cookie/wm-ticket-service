package com.walmart.ticket.system.utils;

import com.walmart.ticket.system.model.Venue;

/**
 * Created by spaladugu on 6/4/2018.
 * Venue seat scoring class that prioritizes seating from left to right and front to back.
     */
    public class SeatScore{

        public float calculateScore(int seatIndex, int rowIndex, int rows, int seatsPerRow) {
            float seatScore = (float)seatsPerRow / (float)(seatsPerRow + seatIndex);
            float rowScore = (float)rows / (float)(rows+ rowIndex);
            return SeatHoldUtils.round((seatScore + rowScore) / 2);

        }
    }
