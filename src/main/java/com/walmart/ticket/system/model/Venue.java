package com.walmart.ticket.system.model;

import com.walmart.ticket.system.utils.SeatScore;

import java.util.Arrays;

/**
 * Venue seating arrangement: assuming 0th row is last row(farthest from screen) in seating.
 * Created by spaladugu on 6/1/2018.
 */
public class Venue {
    private int rows;
    private int seatsPerRow;
    private Seat[][] seats;
    private int capacity;
    private SeatScore scorer;

    public Venue(int rows, int seatsPerRow) {
        super();
        this.rows = rows;
        this.seatsPerRow = seatsPerRow;
        this.capacity = (this.rows * this.seatsPerRow);

        init();
    }

    private void init() {

        seats = new Seat[rows][seatsPerRow];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < seatsPerRow; j++) {
                float score = new SeatScore().calculateScore(i, j, rows, seatsPerRow);
                seats[i][j] = new Seat(new SeatIDX(i, j), score, STATUS.AVAILABLE);

            }
        }
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getSeatsPerRow() {
        return seatsPerRow;
    }

    public void setSeatsPerRow(int seatsPerRow) {
        this.seatsPerRow = seatsPerRow;
    }

    public Seat[][] getSeats() {
        return seats;
    }

    public void setSeats(Seat[][] seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Venue [rows=").append(rows).append(", seatsPerRow=")
                .append(seatsPerRow).append(", ");
        if (seats != null)
            builder.append("seats=").append(Arrays.toString(seats));
        builder.append("]");
        return builder.toString();
    }

    public String prettyPrint() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < seatsPerRow; j++) {
                String s = seats[i][j].getStatus().name().substring(0, 1);
                sb.append(s);
                sb.append(" ");
            }
            sb.append("\n");
        }
//		sb.append("[[ This side stage ]]");
        return sb.toString().trim();
    }


}
