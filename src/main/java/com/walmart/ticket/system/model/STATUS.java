package com.walmart.ticket.system.model;

/**
 * Created by spaladugu on 6/1/2018.
 */

public enum STATUS {
    AVAILABLE, HOLD, RESERVED
}
