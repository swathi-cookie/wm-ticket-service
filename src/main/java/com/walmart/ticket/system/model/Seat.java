package com.walmart.ticket.system.model;

/**
 * Created by spaladugu on 6/1/2018.
 */
public class Seat implements Comparable {
    SeatIDX seatNo;
    Customer reservedBy;
    STATUS status;
    private float score;

    public Seat(SeatIDX seatNo) {
        super();
        this.seatNo = seatNo;
    }

    public Seat(SeatIDX seatNo, STATUS status) {

        this(seatNo);
        this.status = status;
    }

    public Seat(SeatIDX seatNo, float score, STATUS status) {
        this(seatNo);
        this.score = score;
        this.status = status;
    }

    public SeatIDX getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(SeatIDX seatNo) {
        this.seatNo = seatNo;
    }

    public Customer getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(Customer reservedBy) {
        this.reservedBy = reservedBy;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Seat<");
        if (seatNo != null)
            builder.append(seatNo).append(", ");
        if (reservedBy != null)
            builder.append("reservedBy=").append(reservedBy).append(", ");
        if (status != null)
            builder.append("status=").append(status);
        builder.append(">");
        return builder.toString();


        // return String.format("Seat(%d, %d, %.2f)", this.seat, this.row, this.score);

    }

    /*private int seat;
    private int row;
    private float score;
    private String code;

    public Seat(int seat, int row, float score) {
        this.seat = seat;
        this.row = row;
        this.score = score;
        this.code = String.format("%d-%d", this.seat, this.row);
    }
    public Seat(int seat, int row, float score,STATUS status) {
        this.seat = seat;
        this.row = row;
        this.score = score;
        this.status=status;
        this.code = String.format("%d-%d", this.seat, this.row);
    }*/
    @Override
    public int compareTo(Object o) {
        if (o instanceof Seat) {
            return Float.compare(this.score, ((Seat) o).score) * -1;
        } else {
            throw new IllegalArgumentException(o.getClass().getName() + " cannot be compared to " +
                    Seat.class.getName());
        }
    }

    /*public int getSeat() {
        return seat;
    }

    public int getRow() {
        return row;
    }*/

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }


   /* @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Seat) {
            return this.code.equals(((Seat) o).code);
        } else {
            throw new IllegalArgumentException(String.format("Invalid parameter type: %s", o.getClass().getName()));
        }
    }*/
}

	

