## **Ticket Service System**

## Introduction :

Implement a simple ticket service that facilitates the discovery, temporary hold, and final reservation of seats within a high-demand performance venue.
For example, see the seating arrangement below.
 
                            ----------[[  STAGE  ]]---------
                            --------------------------------
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss
                            sssssssssssssssssssssssssssssssss


## Table Of Contents :
        * Module Description
        * Build
        * Deployment
        * Run
        * Test
        * Change Log

1. **Module Description :**

     * Write a Ticket Service that provides the following functions:
     
            * Find the number of seats available within the venue
              Note: available seats are seats that are neither held nor reserved.
              
            * Find and hold the best available seats on behalf of a customer
              Note: each ticket hold should expire within a set number of seconds.
              
            * Reserve and commit a specific group of held seats for a customer

2. **Assumptions**
     * At any given attempt the customer can only hold max(seats per row).
     * If more seats are required to be held then the customer have to make multiple attempts
     * Best seat is calculated at every attempt to hold the seats

3. **Build:**
    * compile, build and install the build result

            mvn clean install
          
    * compile your Java sources

            mvn compile


3. **Deployment:**
    *  Create a deployable JAR file

            mvn clean package

4. **Run:**     

        java -cp target/ticket-service-1.0.0-jar-with-dependencies.jar com.walmart.ticket.system.TicketServiceTool

5. **Change Log:**
    * Initial Commit
    * Updated README file




